package com.example.user.gettyimages.data.remote;

import com.example.user.gettyimages.BuildConfig;
import com.example.user.gettyimages.domain.entities.Result;
import com.example.user.gettyimages.domain.repository.AbstractRepository;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemoteRepository implements AbstractRepository {

    private final Api api;

    public RemoteRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        api = retrofit.create(Api.class);
    }

    @Override
    public Observable<Result> getImages(String query) {
        return api.getImages(BuildConfig.API_KEY, query);
    }
}
