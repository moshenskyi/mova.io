
package com.example.user.gettyimages.domain.entities;

import com.example.user.gettyimages.data.local.entities.RealmImages;
import com.google.gson.annotations.SerializedName;

public class Images {

    @SerializedName("fixed_height_still")
    private FixedHeightStill fixedHeightStill;
    @SerializedName("original_still")
    private OriginalStill originalStill;
    @SerializedName("fixed_width")
    private FixedWidth fixedWidth;
    @SerializedName("fixed_height_small_still")
    private FixedHeightSmallStill fixedHeightSmallStill;
    @SerializedName("fixed_height_downsampled")
    private FixedHeightDownsampled fixedHeightDownsampled;
    @SerializedName("preview")
    private Preview preview;
    @SerializedName("fixed_height_small")
    private FixedHeightSmall fixedHeightSmall;
    @SerializedName("downsized_still")
    private DownsizedStill downsizedStill;
    @SerializedName("downsized")
    private Downsized downsized;
    @SerializedName("downsized_large")
    private DownsizedLarge downsizedLarge;
    @SerializedName("fixed_width_small_still")
    private FixedWidthSmallStill fixedWidthSmallStill;
    @SerializedName("preview_webp")
    private PreviewWebp previewWebp;
    @SerializedName("fixed_width_still")
    private FixedWidthStill fixedWidthStill;
    @SerializedName("480w_still")
    private Image480 Image480;
    @SerializedName("fixed_width_small")
    private FixedWidthSmall fixedWidthSmall;
    @SerializedName("downsized_small")
    private DownsizedSmall downsizedSmall;
    @SerializedName("fixed_width_downsampled")
    private FixedWidthDownsampled fixedWidthDownsampled;
    @SerializedName("downsized_medium")
    private DownsizedMedium downsizedMedium;
    @SerializedName("original")
    private Original original;
    @SerializedName("fixed_height")
    private FixedHeight fixedHeight;
    @SerializedName("looping")
    private Looping looping;
    @SerializedName("original_mp4")
    private OriginalMp4 originalMp4;
    @SerializedName("preview_gif")
    private PreviewGif previewGif;

    public Images(RealmImages images) {
        downsizedMedium = new DownsizedMedium(images.getDownsizedMedium());
    }

    public FixedHeightStill getFixedHeightStill() {
        return fixedHeightStill;
    }

    public void setFixedHeightStill(FixedHeightStill fixedHeightStill) {
        this.fixedHeightStill = fixedHeightStill;
    }

    public OriginalStill getOriginalStill() {
        return originalStill;
    }

    public void setOriginalStill(OriginalStill originalStill) {
        this.originalStill = originalStill;
    }

    public FixedWidth getFixedWidth() {
        return fixedWidth;
    }

    public void setFixedWidth(FixedWidth fixedWidth) {
        this.fixedWidth = fixedWidth;
    }

    public FixedHeightSmallStill getFixedHeightSmallStill() {
        return fixedHeightSmallStill;
    }

    public void setFixedHeightSmallStill(FixedHeightSmallStill fixedHeightSmallStill) {
        this.fixedHeightSmallStill = fixedHeightSmallStill;
    }

    public FixedHeightDownsampled getFixedHeightDownsampled() {
        return fixedHeightDownsampled;
    }

    public void setFixedHeightDownsampled(FixedHeightDownsampled fixedHeightDownsampled) {
        this.fixedHeightDownsampled = fixedHeightDownsampled;
    }

    public Preview getPreview() {
        return preview;
    }

    public void setPreview(Preview preview) {
        this.preview = preview;
    }

    public FixedHeightSmall getFixedHeightSmall() {
        return fixedHeightSmall;
    }

    public void setFixedHeightSmall(FixedHeightSmall fixedHeightSmall) {
        this.fixedHeightSmall = fixedHeightSmall;
    }

    public DownsizedStill getDownsizedStill() {
        return downsizedStill;
    }

    public void setDownsizedStill(DownsizedStill downsizedStill) {
        this.downsizedStill = downsizedStill;
    }

    public Downsized getDownsized() {
        return downsized;
    }

    public void setDownsized(Downsized downsized) {
        this.downsized = downsized;
    }

    public DownsizedLarge getDownsizedLarge() {
        return downsizedLarge;
    }

    public void setDownsizedLarge(DownsizedLarge downsizedLarge) {
        this.downsizedLarge = downsizedLarge;
    }

    public FixedWidthSmallStill getFixedWidthSmallStill() {
        return fixedWidthSmallStill;
    }

    public void setFixedWidthSmallStill(FixedWidthSmallStill fixedWidthSmallStill) {
        this.fixedWidthSmallStill = fixedWidthSmallStill;
    }

    public PreviewWebp getPreviewWebp() {
        return previewWebp;
    }

    public void setPreviewWebp(PreviewWebp previewWebp) {
        this.previewWebp = previewWebp;
    }

    public FixedWidthStill getFixedWidthStill() {
        return fixedWidthStill;
    }

    public void setFixedWidthStill(FixedWidthStill fixedWidthStill) {
        this.fixedWidthStill = fixedWidthStill;
    }

    public Image480 get480wStill() {
        return Image480;
    }

    public void set480wStill(Image480 Image480) {
        this.Image480 = Image480;
    }

    public FixedWidthSmall getFixedWidthSmall() {
        return fixedWidthSmall;
    }

    public void setFixedWidthSmall(FixedWidthSmall fixedWidthSmall) {
        this.fixedWidthSmall = fixedWidthSmall;
    }

    public DownsizedSmall getDownsizedSmall() {
        return downsizedSmall;
    }

    public void setDownsizedSmall(DownsizedSmall downsizedSmall) {
        this.downsizedSmall = downsizedSmall;
    }

    public FixedWidthDownsampled getFixedWidthDownsampled() {
        return fixedWidthDownsampled;
    }

    public void setFixedWidthDownsampled(FixedWidthDownsampled fixedWidthDownsampled) {
        this.fixedWidthDownsampled = fixedWidthDownsampled;
    }

    public DownsizedMedium getDownsizedMedium() {
        return downsizedMedium;
    }

    public void setDownsizedMedium(DownsizedMedium downsizedMedium) {
        this.downsizedMedium = downsizedMedium;
    }

    public Original getOriginal() {
        return original;
    }

    public void setOriginal(Original original) {
        this.original = original;
    }

    public FixedHeight getFixedHeight() {
        return fixedHeight;
    }

    public void setFixedHeight(FixedHeight fixedHeight) {
        this.fixedHeight = fixedHeight;
    }

    public Looping getLooping() {
        return looping;
    }

    public void setLooping(Looping looping) {
        this.looping = looping;
    }

    public OriginalMp4 getOriginalMp4() {
        return originalMp4;
    }

    public void setOriginalMp4(OriginalMp4 originalMp4) {
        this.originalMp4 = originalMp4;
    }

    public PreviewGif getPreviewGif() {
        return previewGif;
    }

    public void setPreviewGif(PreviewGif previewGif) {
        this.previewGif = previewGif;
    }

}
