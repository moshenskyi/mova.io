package com.example.user.gettyimages.data.local.entities;

import com.example.user.gettyimages.domain.entities.DownsizedMedium;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmDownsizedMedium extends RealmObject {
    private String url;

    public RealmDownsizedMedium() {
    }

    public RealmDownsizedMedium(DownsizedMedium downsizedMedium) {
        url = downsizedMedium.getUrl();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
