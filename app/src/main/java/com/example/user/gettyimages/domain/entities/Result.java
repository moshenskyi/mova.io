package com.example.user.gettyimages.domain.entities;

import com.example.user.gettyimages.data.local.entities.RealmDatum;
import com.example.user.gettyimages.data.local.entities.RealmResult;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class Result {

    @SerializedName("data")
    private List<Datum> data;
    @SerializedName("pagination")
    private Pagination pagination;
    @SerializedName("meta")
    private Meta meta;

    public Result(RealmResult realmResult) {
        if (realmResult != null) {
            RealmList<RealmDatum> data = realmResult.getData();
            this.data = new ArrayList<>();
            for (RealmDatum datum : data) {
                this.data.add(new Datum(datum));
            }
        }
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

}
