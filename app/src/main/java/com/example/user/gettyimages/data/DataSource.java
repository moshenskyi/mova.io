package com.example.user.gettyimages.data;

import com.example.user.gettyimages.data.local.ILocalRepository;
import com.example.user.gettyimages.domain.entities.Result;
import com.example.user.gettyimages.domain.repository.AbstractRepository;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DataSource {
    private ILocalRepository localRepository;
    private AbstractRepository remoteRepository;

    public DataSource(ILocalRepository localRepository, AbstractRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }

    public Observable<Result> getImages(String query) {
        return localRepository.getImages(query)
                .subscribeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    return remoteRepository.getImages(query)
                            .doOnNext(result -> localRepository.saveResult(result, query)
                                    .subscribeOn(AndroidSchedulers.mainThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe())
                            .onErrorResumeNext((Throwable remoteError) -> {
                                throw new Exception(throwable.getMessage()
                                        + " Check your internet connection.");
                            })
                            .subscribeOn(Schedulers.io());
                });
    }
}
