package com.example.user.gettyimages.data.local.entities;

import com.example.user.gettyimages.domain.entities.Datum;
import com.example.user.gettyimages.domain.entities.Result;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmResult extends RealmObject {
    @PrimaryKey
    private long id;
    private RealmList<RealmDatum> data;
    private String query;

    public RealmResult() {
    }

    public RealmResult(Result result, String query) {
        this.data = new RealmList<>();

        List<Datum> data = result.getData();
        for (Datum datum : data) {
            this.data.add(new RealmDatum(datum));
        }

        this.query = query;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RealmList<RealmDatum> getData() {
        return data;
    }

    public void setData(RealmList<RealmDatum> data) {
        this.data = data;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
