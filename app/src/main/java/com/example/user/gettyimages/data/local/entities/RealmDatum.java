package com.example.user.gettyimages.data.local.entities;

import com.example.user.gettyimages.domain.entities.Datum;
import com.example.user.gettyimages.domain.entities.Images;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmDatum extends RealmObject {

    private RealmImages images;

    public RealmDatum() {
    }

    public RealmDatum(Datum datum) {
        Images images = datum.getImages();
        this.images = new RealmImages(images);
    }

    public RealmImages getImages() {
        return images;
    }

    public void setImages(RealmImages images) {
        this.images = images;
    }
}
