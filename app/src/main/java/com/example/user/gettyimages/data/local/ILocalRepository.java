package com.example.user.gettyimages.data.local;

import com.example.user.gettyimages.data.local.entities.RealmResult;
import com.example.user.gettyimages.domain.entities.Result;
import com.example.user.gettyimages.domain.repository.AbstractRepository;

import io.reactivex.Observable;

public interface ILocalRepository extends AbstractRepository {
    Observable<RealmResult> saveResult(Result result, String query);
}
