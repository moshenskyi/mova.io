package com.example.user.gettyimages.data.local;

import com.example.user.gettyimages.data.local.entities.RealmResult;
import com.example.user.gettyimages.domain.entities.Result;

import io.reactivex.Observable;
import io.realm.Realm;

public class LocalRepository implements ILocalRepository {

    private final ResultDao resultDao;

    public LocalRepository() {
        resultDao = new ResultDao(Realm.getDefaultInstance());
    }

    @Override
    public Observable<Result> getImages(String query) {
        return resultDao.getImages(query);
    }

    @Override
    public Observable<RealmResult> saveResult(Result result, String query) {
        return resultDao.saveResult(new RealmResult(result, query));
    }

}
