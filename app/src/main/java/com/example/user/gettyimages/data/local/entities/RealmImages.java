package com.example.user.gettyimages.data.local.entities;

import com.example.user.gettyimages.domain.entities.Images;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmImages extends RealmObject {
    private RealmDownsizedMedium downsizedMedium;

    public RealmImages() {
    }

    public RealmImages(Images images) {
        downsizedMedium = new RealmDownsizedMedium(images.getDownsizedMedium());
    }


    public RealmDownsizedMedium getDownsizedMedium() {
        return downsizedMedium;
    }

    public void setDownsizedMedium(RealmDownsizedMedium downsizedMedium) {
        this.downsizedMedium = downsizedMedium;
    }
}
