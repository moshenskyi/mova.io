package com.example.user.gettyimages.data.local;

import android.support.annotation.NonNull;

import com.example.user.gettyimages.data.local.entities.RealmResult;
import com.example.user.gettyimages.domain.entities.Result;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.realm.Realm;
import io.realm.RealmResults;

public class ResultDao {
    private final Realm realmDb;

    ResultDao(Realm realmDb) {
        this.realmDb = realmDb;
    }

    Observable<RealmResult> saveResult(RealmResult result) {
        return Observable.create(emitter -> {
            saveTransaction(result);
        });
    }

    private void saveTransaction(RealmResult result) {
        realmDb.beginTransaction();
        long id = getId();
        RealmResults<RealmResult> existedResult = realmDb.where(RealmResult.class)
                .equalTo("query", result.getQuery()).findAll();
        if (existedResult.isEmpty()) {
            result.setId(id);
            realmDb.copyToRealm(result);
        }

        realmDb.commitTransaction();
    }

    private long getId() {
        Number idField = realmDb.where(RealmResult.class).max("id");
        return idField == null ? 0 : idField.longValue() + 1;
    }

    Observable<Result> getImages(String query) {
        return Observable.create(emitter -> {
            RealmResults<RealmResult> resultRealm = retrieveDataFromDb(query);
            if (!resultRealm.isEmpty()) {
                emitResults(emitter, resultRealm);
            } else {
                throw new Exception("No data found.");
            }
        });
    }

    private void emitResults(ObservableEmitter<Result> emitter, RealmResults<RealmResult> resultRealm) {
        Result result = getResult(resultRealm);
        emitter.onNext(result);
    }

    @NonNull
    private Result getResult(RealmResults<RealmResult> resultRealm) {
        RealmResult data = resultRealm.first();
        return new Result(data);
    }

    private RealmResults<RealmResult> retrieveDataFromDb(String query) {
        return realmDb
                .where(RealmResult.class)
                .equalTo("query", query)
                .findAll();
    }
}
