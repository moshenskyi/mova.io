package com.example.user.gettyimages.presentation.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.user.gettyimages.data.DataSource;
import com.example.user.gettyimages.domain.entities.Datum;
import com.example.user.gettyimages.domain.entities.Meta;
import com.example.user.gettyimages.domain.entities.Result;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.exceptions.CompositeException;

public class GifListViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private DataSource dataSource;
    private MutableLiveData<List<Datum>> data = new MutableLiveData<>();
    private MutableLiveData<String> error = new MutableLiveData<>();

    public void addDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public MutableLiveData<List<Datum>> getData() {
        return data;
    }

    public MutableLiveData<String> getError() {
        return error;
    }

    public void loadImages(String query) {
        disposables.add(dataSource.getImages(query)
                .filter(this::filterData)
                .map(Result::getData)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::showResults,
                        this::showError,
                        disposables::clear)
        );
    }

    private boolean filterData(Result result) throws Exception {
        Meta meta = result.getMeta();
        if (meta != null && meta.getStatus() > 400) {
            throw new Exception(meta.getMsg());
        } else return true;
    }

    private void showResults(List<Datum> images) {
        if (images.isEmpty())
            error.postValue("No data found.");
        data.postValue(images);
    }

    private void showError(Throwable error) {
        if (error instanceof CompositeException) {
            showLastErrorFromStackTrace((CompositeException) error);
        } else {
            GifListViewModel.this.error.postValue(error.getMessage());
        }
    }

    private void showLastErrorFromStackTrace(CompositeException error) {
        List<Throwable> exceptions = error.getExceptions();
        int exceptionCount = exceptions.size();
        String message = exceptions.get(exceptionCount - 1).getMessage();
        GifListViewModel.this.error.postValue(message);
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }
}
