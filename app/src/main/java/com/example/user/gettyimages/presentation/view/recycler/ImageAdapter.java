package com.example.user.gettyimages.presentation.view.recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.gettyimages.R;
import com.example.user.gettyimages.domain.entities.Datum;
import com.example.user.gettyimages.domain.entities.DownsizedMedium;
import com.example.user.gettyimages.domain.entities.Images;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageHolder> {
    private final List<Datum> imageItems = new ArrayList<>();
    private String query;

    public void update(List<Datum> items, String query) {
        if (items != null) {
            this.query = query;

            imageItems.clear();
            imageItems.addAll(items);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        View root = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item, viewGroup, false);
        return new ImageHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolder imageHolder, int position) {
        setupPhoto(imageHolder, position);

        imageHolder.searchQuery.setText(query);
    }

    private void setupPhoto(@NonNull ImageHolder imageHolder, int position) {
        Datum datum = imageItems.get(position);
        Images images = datum.getImages();
        DownsizedMedium downsizedMedium = images.getDownsizedMedium();
        if (downsizedMedium != null) {
            Glide.with(imageHolder.itemView)
                    .load(downsizedMedium.getUrl())
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                    .apply(RequestOptions.placeholderOf(R.drawable.img_not_found))
                    .apply(RequestOptions.centerCropTransform())
                    .into(imageHolder.gifImage);

        }
    }

    @Override
    public int getItemCount() {
        return imageItems.size();
    }

    class ImageHolder extends RecyclerView.ViewHolder {
        private ImageView gifImage;
        private TextView searchQuery;

        ImageHolder(@NonNull View itemView) {
            super(itemView);

            gifImage = itemView.findViewById(R.id.gif_image);
            searchQuery = itemView.findViewById(R.id.search_query);
        }
    }
}
