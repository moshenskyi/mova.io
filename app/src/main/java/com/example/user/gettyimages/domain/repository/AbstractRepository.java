package com.example.user.gettyimages.domain.repository;

import com.example.user.gettyimages.domain.entities.Result;

import io.reactivex.Observable;

public interface AbstractRepository {
    Observable<Result> getImages(String query);
}
