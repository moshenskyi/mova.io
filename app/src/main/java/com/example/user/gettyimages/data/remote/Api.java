package com.example.user.gettyimages.data.remote;

import com.example.user.gettyimages.domain.entities.Result;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    @GET("search")
    Observable<Result> getImages(@Query("api_key") String apiKey, @Query("q") String query);

}
