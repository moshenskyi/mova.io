package com.example.user.gettyimages.presentation.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.gettyimages.R;
import com.example.user.gettyimages.data.DataSource;
import com.example.user.gettyimages.data.local.LocalRepository;
import com.example.user.gettyimages.data.remote.RemoteRepository;
import com.example.user.gettyimages.domain.entities.Datum;
import com.example.user.gettyimages.presentation.Utils;
import com.example.user.gettyimages.presentation.view.recycler.ImageAdapter;
import com.example.user.gettyimages.presentation.view.recycler.SpaceItemDecoration;
import com.example.user.gettyimages.presentation.viewmodel.GifListViewModel;

import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    private RecyclerView gifList;
    private EditText searchField;

    private String query;

    private ImageAdapter adapter;
    private GifListViewModel gifListViewModel;

    private TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                search(searchField.getText().toString());
                Utils.hideKeyboard(MainActivity.this);
                return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUi();
        setupRecycler();

        initViewModel();
    }

    private void initUi() {
        gifList = findViewById(R.id.gif_list);

        searchField = findViewById(R.id.search_field);
        searchField.setOnEditorActionListener(onEditorActionListener);
    }

    private void search(String query) {
        this.query = query;
        gifListViewModel.loadImages(query);
    }

    private void initViewModel() {
        gifListViewModel = ViewModelProviders.of(this).get(GifListViewModel.class);

        gifListViewModel.addDataSource(new DataSource(
                new LocalRepository(),
                new RemoteRepository()));

        gifListViewModel.getData().observe(this, this::updateAdapter);
        gifListViewModel.getError().observe(this, this::showMessage);
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void updateAdapter(List<Datum> datum) {
        adapter.update(datum, query);
    }

    private void setupRecycler() {
        gifList.setLayoutManager(new GridLayoutManager(this, 2));

        adapter = new ImageAdapter();
        gifList.setAdapter(adapter);

        gifList.addItemDecoration(new SpaceItemDecoration(getDimension()));
    }

    private int getDimension() {
        return (int) getResources().getDimension(R.dimen.item_padding);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Realm.getDefaultInstance().close();
    }
}
